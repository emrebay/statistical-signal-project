import numpy as np
import imageio
import matplotlib.pyplot as plt
import numpy as np
import pdb

def im2patch(im,s):
    k=0;
    psize=(s*s,(im.shape[0]-s+1)*(im.shape[1]-s+1))
    disfc=np.floor(s/2)
    pa=np.zeros(psize)
    paidx=np.zeros((2,(im.shape[0]-s+1)*(im.shape[1]-s+1)))
    for i in range(im.shape[0]-s+1):
        for j in range(im.shape[1]-s+1):
            panr=im[i:i+s,j:j+s]
            pa[:,k]=panr.reshape(s*s)
            paidx[:,k]=[i,j]
            k=k+1
    cpidxf=paidx+disfc
    cpidx=cpidxf.astype('int')
    return pa,paidx,cpidx

def patch2im(sizeim,pa,paidx,s):
    im=np.zeros(sizeim)
    imw=np.zeros(sizeim)
    paidx=paidx.astype('int')
    for i in range(paidx.shape[1]):
        pasq=pa[:,i].reshape([s,s])
        idx=paidx[:,i]
        im[idx[0]:idx[0]+s,idx[1]:idx[1]+s]=im[idx[0]:idx[0]+s,idx[1]:idx[1]+s]+pasq
        imw[idx[0]:idx[0]+s,idx[1]:idx[1]+s]=imw[idx[0]:idx[0]+s,idx[1]:idx[1]+s]+1
    reconimage=im/imw
    return reconimage      

def paclustering(pa,cpidx,cln,nnn):
    [pacnt,idxcnt]=cmpclctr(pa,cpidx,cln)
    nncl=nearestcnt(cpidx,idxcnt,nnn)
    nncl=nncl.astype('int')
    for i in range(pa.shape[1]):
        nbrep=nncl.shape[0]
        selpac=pacnt[:,nncl[:,i].flatten()]
        Matpa=np.tile(pa[:,i:i+1],(1,nbrep))
        distant=np.linalg.norm(Matpa-selpac,axis=0)
        argcl=np.argmin(distant)
        cln[i]=nncl[argcl,i]
    return cln

def nearestcnt(idx,idxcnt,nnn):
    nncl=np.zeros((nnn,idx.shape[1]))
    for i in range(idx.shape[1]):
        nbrep=idxcnt.shape[1]
        Matidx=np.tile(idx[:,i:i+1],(1,nbrep))
        distant=np.linalg.norm(Matidx-idxcnt,axis=0)
        clnnn=np.argpartition(distant,nnn)[:nnn]
        nncl[:,i]=clnnn
    return nncl
    

def cmpclctr(pa,cpaidx,cln):
    ncl=max(cln).astype('int')
    cln=cln.flatten()
    pacenter=np.zeros((pa.shape[0],ncl[0]))
    idxcenter=np.zeros((cpaidx.shape[0],ncl[0]))
    for i in range(ncl[0]):
        pacenter[:,i]=np.mean(pa[:,cln==i],axis=1)
        idxcenter[:,i]=np.mean(cpaidx[:,cln==i],axis=1)
    return pacenter,idxcenter    
    

def spclustering(idx,sizeim):
    a1=np.arange(1,sizeim[0],20)
    a2=np.arange(1,sizeim[1],20)
    clidx=np.array(np.meshgrid(a1,a2)).T.reshape(-1,2).T
    cln=np.zeros([idx.shape[1],1])
    for i in range(idx.shape[1]):
        nbrep=clidx.shape[1]
        Matidx=np.tile(idx[:,i:i+1],(1,nbrep))
        distant=np.linalg.norm(Matidx-clidx,axis=0)
        cln[i,0]=np.argmin(distant)
    return cln

def colabfilter(npa,prepa,cln,sigma):
    cln=cln.flatten()
    numcl=np.max(cln).astype('int')
    dpa=np.zeros(npa.shape)
    for i in range(numcl):
        npac=npa[:,cln==i]
        prepac=prepa[:,cln==i]
     #   dpac=Gdenoise(npac,prepac,sigma)
        dpac=svddenoise(npac,sigma,35)
        dpa[:,cln==i]=dpac
    return dpa


def Gdenoise(npa,estpa,sigma):
    s=npa.shape[0]
    meanc=np.mean(estpa,axis=1)
    covc=computecov(estpa)+.0001*np.eye(s)
    invcovc=np.linalg.inv(covc)
    Wp=np.linalg.inv(invcovc+1/(sigma**2)*np.eye(s))
    Mcr=np.tile(invcovc.dot(meanc),(npa.shape[1],1)).T
    dpac=Wp.dot(npa/(sigma**2)+Mcr)
    return dpac

def svddenoise(npa,sigma,tre):
    dim0=npa.shape[0]
    dim1=npa.shape[1]
    U, s, Vh=np.linalg.svd(npa)
    stre=s
    stre[stre<tre*sigma]=0;
    smat=np.zeros((dim0,dim1))
    smat[:dim0,:dim0]=np.diag(stre)
    dpa=np.dot(U,np.dot(smat,Vh))
    return dpa
    
def computecov(data):
    s=data.shape[0]
    acc=np.zeros((s,s))
    mudata=np.mean(data,axis=1)
    for i in range(data.shape[1]):
        veccor=np.outer(data[:,i]-mudata,data[:,i]-mudata)
        acc=acc+veccor
    return acc/data.shape[1]


img=imageio.imread('house.png');
imgcl=np.zeros(img.shape)
sigma=20
s=8
#paidx:patch idx, cpix: centeral pixel of patch
imgn=img+sigma*np.random.normal(size=img.shape)
[npa,paidx,cpidx]=im2patch(imgn,s)
cln=spclustering(paidx,imgn.shape)
dpa=npa
for i in range(1):
    cln=paclustering(dpa,cpidx,cln,5)
    dpa=colabfilter(npa,dpa,cln,sigma)
    
rimg=patch2im(imgn.shape,dpa,paidx,s)
psnr1=10*np.log10(255**2/np.mean((imgn-img)**2))
psnr2=10*np.log10(255**2/np.mean((rimg-img)**2))
plt.figure(1)
w1=plt.subplot(121)
w1.set_title('Noisy image, PSNR=%.2f'%(psnr1))
plt.imshow(imgn,cmap='gray')
w2=plt.subplot(122)
w2.set_title('Denoised Image, PSNR=%.2f' %(psnr2))
plt.imshow(rimg,cmap='gray')
plt.savefig('cameraman_denoised.jpg')
plt.show()